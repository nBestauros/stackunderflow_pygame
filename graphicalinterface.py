import pygame


# -------------------------
# assets
# -------------------------
pygame.font.init()

#for i in range(len(pygame.font.get_fonts())):
#    print(pygame.font.get_fonts()[i])


# colors
black = (0, 0, 0)
white = (255, 255, 255)
gold = (255, 223, 0)
green = (0, 255, 0)
magenta = (255, 0 , 255)
screen = pygame.display.set_mode((600, 600))


class GraphicalInterface:
    def __init__(self):
        self.titleCardFont = pygame.font.SysFont("agencyfb", 63)
        self.mainMenuFont = pygame.font.SysFont("franklingothicbook", 30)
        self.hintFont = pygame.font.SysFont("franklingothicbook", 12)

    def displaymainmenu(self, pos):
        # title card bg
        screen.fill(black)
        bg = pygame.Surface((350, 80))
        bg.fill(white)
        bgrect = bg.get_rect()
        screen.blit(bg, (125, 75))

        title = self.titleCardFont.render("Hunt The Wumpus", True, black)

        if pos == 0:
            caveselect = self.mainMenuFont.render("Cave Select", True, gold)
        else:
            caveselect = self.mainMenuFont.render("Cave Select", True, white)
        caveselectsize = self.mainMenuFont.size("Cave Select")

        screen.blit(title, (130, 75))

        self.draw(caveselect, 300, 200, caveselectsize)
        if pos == 1:
            highscore = self.mainMenuFont.render("View High Scores", True, gold)
        else:
            highscore = self.mainMenuFont.render("View High Scores", True, white)
        highscoresize = self.mainMenuFont.size("View High Scores")

        self.draw(highscore, 300, 250, highscoresize)

        helpdisplay = self.hintFont.render("Use the up and down arrows to navigate the menu.", True, white)
        self.draw(helpdisplay, 300, 450, self.hintFont.size("Use the up and down arrows to navigate the menu."))

        helpdisplay = self.hintFont.render("Hit enter to select an option.", True, white)
        self.draw(helpdisplay, 300, 475, self.hintFont.size("Hit enter to select an option."))

        pygame.display.flip()

    def displaycaveselectionscreen(self, pos):
        screen.fill(black)
        bg = pygame.Surface((350, 80))
        bg.fill(white)
        bgrect = bg.get_rect()
        screen.blit(bg, (125, 75))

        title = self.titleCardFont.render("Cave Selection", True, black)
        screen.blit(title, (155, 75))

        for i in range (5):
            if i == pos:
                caveSelect = self.mainMenuFont.render("Cave " + str(i+1), True, gold)
                self.draw(caveSelect, 300, 250+i*50, self.mainMenuFont.size("Cave "+str(i)))
            else:
                caveSelect = self.mainMenuFont.render("Cave " + str(i+1), True, white)
                self.draw(caveSelect, 300, 250 + i * 50, self.mainMenuFont.size("Cave " + str(i)))

        helpdisplay = self.hintFont.render("Use the up and down arrows to navigate the menu.", True, white)
        self.draw(helpdisplay, 300, 500, self.hintFont.size("Use the up and down arrows to navigate the menu."))

        helpdisplay = self.hintFont.render("Hit enter to select an option.", True, white)
        self.draw(helpdisplay, 300, 525, self.hintFont.size("Hit enter to select an option."))

        pygame.display.flip()

    def displayhighscores(self, scores):
        screen.fill(black)
        bg = pygame.Surface((350, 80))
        bg.fill(white)
        bgrect = bg.get_rect()
        screen.blit(bg, (125, 75))

        title = self.titleCardFont.render("High Scores", True, black)
        self.draw(title, 300, 110, self.titleCardFont.size("High Scores"))
        # screen.blit(title, (155, 75))

        info = self.hintFont.render("Score Name Cave Turns Coins Arrows Killed", True, white)
        self.draw(info, 300, 200, self.hintFont.size("Score Name Cave Turns Coins Arrows Killed"))

        for i in range(len(scores)):
            caveSelect = self.hintFont.render("Score " + str(i + 1) + ": " + scores[i].rstrip(), True, white)
            self.draw(caveSelect, 300, 250 + i * 30, self.hintFont.size("Score " + str(i) + ": " + scores[i].rstrip()))

        helpdisplay = self.hintFont.render("Hit Backspace to go back.", True, white)
        self.draw(helpdisplay, 300, 450, self.hintFont.size("Hit Backspace to go back."))

        pygame.display.flip()

    def displaytempgame(self):
        screen.fill(black)
        bg = pygame.Surface((350, 80))
        bg.fill(white)
        bgrect = bg.get_rect()
        screen.blit(bg, (125, 75))

        title = self.titleCardFont.render("Temp Game", True, black)
        self.draw(title, 300, 110, self.titleCardFont.size("Temp Game"))

        pygame.display.flip()

    def displayname(self, name, iserror):
        screen.fill(black)
        bg = pygame.Surface((350, 80))
        bg.fill(white)
        bgrect = bg.get_rect()
        screen.blit(bg, (125, 75))

        title = self.titleCardFont.render("Enter Name", True, black)
        self.draw(title, 300, 110, self.titleCardFont.size("Enter Name"))

        text = self.mainMenuFont.render(name, True, gold)
        self.draw(text, 300, 250, self.mainMenuFont.size(name))

        if iserror:
            self.displaynameerror()

        helpdisplay = self.hintFont.render("Hit enter to continue.", True, white)
        self.draw(helpdisplay, 300, 475, self.hintFont.size("Hit enter to continue."))
        pygame.display.flip()

    def displaynameerror(self):
        title = self.titleCardFont.render("Name can't be empty", True, white)
        self.draw(title, 300, 400, self.titleCardFont.size("Name can't be empty"))

    # Draw Method (draws in center)
    def draw(self, text, x, y, caveselectsize):
        drawX = x - (caveselectsize[0] / 2.)
        drawY = y - (caveselectsize[1] / 2.)
        coords = (drawX, drawY)
        screen.blit(text, coords)
    # Draw arrow (draws in center)
    def drawArrow(self, direction):
        arrow = pygame.image.load("arrow.png").convert_alpha()
        arrow = pygame.transform.rotate(arrow, direction*-60)
        arrowsize = arrow.get_size()
        drawX = 300 - (arrowsize[0] / 2.)
        drawY = 300 - (arrowsize[1] / 2.)
        coords = (drawX, drawY)
        screen.blit(arrow, coords)


    def drawCave(self):
        cave = pygame.image.load("bg.jpg").convert_alpha()
        cavesize = cave.get_size()
        drawX = 300 - (cavesize[0] / 2.)
        drawY = 300 - (cavesize[1] / 2.)
        coords = (drawX, drawY)
        screen.blit(cave, coords)

    def drawtunnel(self, tunnelnum, roomnum):
        tunnel = pygame.image.load(str(tunnelnum)+".png").convert_alpha()
        screen.blit(tunnel, (0, 0))
        numcoords = [[295, 50], [542, 158], [542, 439], [300, 550], [57, 439], [57, 158]]

        text = self.mainMenuFont.render(str(roomnum), True, gold)
        self.draw(text, numcoords[tunnelnum][0], numcoords[tunnelnum][1], self.mainMenuFont.size(str(roomnum)))

    def displayCave(self, currentroom, connections, direction, numcoins, turnnum, arrowsleft):

        screen.fill(black)
        self.drawCave()
        self.drawArrow(direction)

        for i in range(6):
            if connections[currentroom-1][i] > 0:
                self.drawtunnel(i, connections[currentroom-1][i])

        text = self.mainMenuFont.render(str(currentroom), True, gold)
        self.draw(text, 300, 350, self.mainMenuFont.size(str(currentroom)))


        coins = self.mainMenuFont.render("Coins: "+str(numcoins), True, gold)
        self.draw(coins, 520, 30, self.mainMenuFont.size("Coins: "+str(numcoins)))

        turns = self.mainMenuFont.render("Turns: "+str(turnnum), True, white)
        self.draw(turns, 520, 570, self.mainMenuFont.size("Turns: "+str(turnnum)))

        #controls = self.mainMenuFont.render("Controls:", True, gold)
        #self.draw(controls, 65, 450, self.mainMenuFont.size("Controls:"))

        actualcontrolsup = self.hintFont.render("Arrow Trivia: U", True, white)
        self.draw(actualcontrolsup, 98, 510, self.mainMenuFont.size("Arrow Trivia: U"))

        actualcontroldown = self.hintFont.render("Secret Trivia: K", True, white)
        self.draw(actualcontroldown, 104, 530, self.mainMenuFont.size("Secret Trivia: K"))

        actualcontrolleft = self.hintFont.render("Turn Left: A", True, white)
        self.draw(actualcontrolleft, 82, 550, self.mainMenuFont.size("Turn Left: A"))

        actualcontrolright = self.hintFont.render("Turn Right: D", True, white)
        self.draw(actualcontrolright, 90, 570, self.mainMenuFont.size("Turn Right: D"))

        actualcontrolforward = self.hintFont.render("Enter Room: Spacebar", True, white)
        self.draw(actualcontrolforward, 150, 590, self.mainMenuFont.size("Enter Room: Spacebar"))

        shootarrowcontrol = self.hintFont.render("Shoot Arrow: X", True, white)
        self.draw(shootarrowcontrol, 190,580, self.hintFont.size("Shoot Arrow: X"))

        arrowstext = "Arrows: " +str(arrowsleft)
        arrowdisplay = self.mainMenuFont.render(arrowstext, True, white)
        self.draw(arrowdisplay, 80, 30, self.mainMenuFont.size(arrowstext))

        pygame.display.flip()

    def displayHint(self, hint):
        text = self.hintFont.render(hint, True, white)
        self.draw(text, 300, 400, self.hintFont.size(hint))
        pygame.display.flip()

    def displaySecret(self, secret):
        text = self.hintFont.render(secret, True, green)
        self.draw(text, 300, 270, self.hintFont.size(secret))
        pygame.display.flip()

    def displayWarning(self, warnings):
        for i in range(len(warnings)):
            warning = self.mainMenuFont.render(warnings[i], True, magenta)
            self.draw(warning, 300, 200+i*50, self.mainMenuFont.size(warnings[i]))
        pygame.display.flip()

    def displaytrivia(self, question, numcorrect, answers, questionnum, triviatype):
        screen.fill(black)
        bg = pygame.Surface((350, 80))
        bg.fill(white)
        bgrect = bg.get_rect()
        screen.blit(bg, (125, 75))

        title = self.titleCardFont.render("Trivia", True, black)
        screen.blit(title, (255, 75))

        # displays question
        question_width, question_height = self.mainMenuFont.size(question.rstrip())

        if question_width>550:
            question = question.split()
            string1 = ""
            string2 = ""
            if len(question)%2==0:
                for i in range(int(len(question)/2)):
                    string1 =  string1 + question[i] + " "
                for i in range(int(len(question)/2)):
                    string2 = string2 + question[int(len(question)/2) +i] +" "
            elif len(question)%2==1:
                for i in range(int((len(question)/2)+1)):
                    string1 = string1 + question[i] + " "
                for i in range(int(len(question)/2)):
                    string2 = string2 + question[int(len(question)/2) +i + 1] +" "

            displayquestion1 = self.mainMenuFont.render(string1.rstrip(), True, gold)
            self.draw(displayquestion1, 300, 200, self.mainMenuFont.size(string1.rstrip()))
            displayquestion2 = self.mainMenuFont.render(string2.rstrip(), True, gold)
            self.draw(displayquestion2, 300, 250, self.mainMenuFont.size(string2.rstrip()))
        else:
            displayquestion = self.mainMenuFont.render(question.rstrip(), True, gold)
            self.draw(displayquestion, 300, 200, self.mainMenuFont.size(question.rstrip()))

        for i in range(4):
            answer = self.hintFont.render(str(i+1) +": "+ answers[i].rstrip(), True, white)
            self.draw(answer, 300, 300 + i * 50, self.hintFont.size(str(i+1) +": "+ answers[i].rstrip()))

        helps = self.mainMenuFont.render("Hit keys 1-4 to answer the trivia question.", True, white)
        self.draw(helps, 300, 550, self.mainMenuFont.size("Hit keys 1-4 to answer the trivia question."))

        if triviatype == "wumpus":
            nums = 5
        else:
            nums = 3

        # displays question number
        questionstring = str(questionnum) + "/" + str(nums)
        questiondisplay = self.titleCardFont.render(questionstring, True, white)
        self.draw(questiondisplay, 550, 50, self.titleCardFont.size(questionstring))

        #displays correct number
        correctstring = str(numcorrect) + "/" + str(questionnum)
        correctdisplay = self.titleCardFont.render(correctstring, True, green)
        self.draw(correctdisplay, 550, 100, self.titleCardFont.size(correctstring))

        pygame.display.flip()


    def displayLossScreen(self, reason):
        screen.fill(black)
        bg = pygame.Surface((350, 80))
        bg.fill(white)
        bgrect = bg.get_rect()
        screen.blit(bg, (125, 75))

        title = self.titleCardFont.render("You Lost!", True, black)
        screen.blit(title, (205, 75))

        lossdisplay = self.titleCardFont.render(reason, True, white)
        self.draw(lossdisplay, 300, 300, self.titleCardFont.size(reason))

        helpdisplay = self.hintFont.render("Hit the Spacebar to continue to highscores.", True, white)
        self.draw(helpdisplay, 300, 450, self.hintFont.size("Hit the Spacebar to continue to highscores."))


        pygame.display.flip()

    def displayWinScreen(self):
        screen.fill(black)
        bg = pygame.Surface((350, 80))
        bg.fill(white)
        bgrect = bg.get_rect()
        screen.blit(bg, (125, 75))

        title = self.titleCardFont.render("You Won!", True, black)
        screen.blit(title, (205, 75))

        lossdisplay = self.titleCardFont.render("You killed the Wumpus!", True, white)
        self.draw(lossdisplay, 300, 300, self.titleCardFont.size("You killed the Wumpus!"))

        helpdisplay = self.hintFont.render("Hit the Spacebar to continue to highscores.", True, white)
        self.draw(helpdisplay, 300, 450, self.hintFont.size("Hit the Spacebar to continue to highscores."))

        pygame.display.flip()

