import random


class Trivia:
    def __init__(self):
        self.questions = []
        self.answers = []
        file = open("WumpusQuestions.txt", "+r")
        for i in range (98):
            tempanswers = []
            for x in range(6):
                if x == 0:
                    self.questions.append(file.readline())
                elif x < 5:
                    tempanswers.append(file.readline())
                else:
                    file.readline()
            self.answers.append(tempanswers)
        self.index = random.randrange(60)

    def debug(self):
        for i in range(98):
            print("question #" + str(i) + self.questions[i])
            for x in range(4):
                print(self.answers[i][x])

    def getquestion(self):
        return self.questions[self.index]

    def getshuffeledanswers(self):
        temp = list.copy(self.answers[self.index])
        random.shuffle(temp)
        # print("shuffled")
        # (temp)
        return temp

    def gethint(self):
        num = random.randrange(98)
        return self.questions[num].rstrip() + " " + self.answers[num][0].rstrip()

    def getorderedanswers(self):

        return self.answers[self.index]

    def incrementindex(self):
        if self.index<97:
            self.index = self.index+1
        else:
            self.index = 0

    def checkanswer(self, answer):
        # print("Answer: "+ answer.rstrip() + " Correct: "+self.answers[self.index][0].rstrip())
        if answer.rstrip() == self.answers[self.index][0].rstrip():
            self.incrementindex()
            return True
        else:
            self.incrementindex()
            return False

    def getOldTrivia(self):
        return self.questions[self.index].rstrip()+" "+self.answers[self.index][0].rstrip()
