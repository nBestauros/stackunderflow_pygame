import pygame


class Cave(pygame.sprite.Sprite):
    def __init__(self, cavenum):
        self.caveNum = cavenum
        self.connections = []
        # connections.append and connections.insert(index, elem)
        self.readcave(cavenum)

    def readcave(self, cavenum):
        cave = open(str(cavenum)+".txt")
        for i in range(30):
            temp = cave.readline()
            self.connections.append(temp.split())
            # print(self.connections[i])
        for i in range(30):
            for x in range(6):
                self.connections[i][x] = int(self.connections[i][x])
            # print(self.connections[i])
        cave.close()

    def getCave(self):
        return self.connections

    def tostring(self):
        return "cave"
