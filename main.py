# python version 3.7.0
# pygame version 1.9.6
import pygame
import player
import graphicalinterface
import cave
import gamelocations
import trivia
import highscore
import random
from pygame.locals import *

# game states
running = True
mainMenu = True
gameScreen = False
wumpusTriviaScreen = False
pitTriviaScreen = False
viewHighScoreScreen = False
newHighScoreScreen = False
caveSelectionScreen = False
enterNameScreen = False
youLostScreen = False
youWonScreen = False
buyArrowScreen = False
buySecretScreen = False



# objects
# make player when the game starts
# player = player.Player("")
gi = graphicalinterface.GraphicalInterface()
currentgl = gamelocations.GameLocations()
currenttrivia = trivia.Trivia()
hs = highscore.HighScore()

#extra
secretclock = 0
wumpusranawayclock = 0
noarrowclock = 0
nosecretclock = 0
wumpusmissclock = 0
hint = currenttrivia.gethint()

lossReason = ""

# initialize pygame
pygame.init()
while running:
    clock = pygame.time.Clock()
    clock.tick(60)
    # Our main loop
    pos = 0
    while running and mainMenu:
        gi.displaymainmenu(pos)
        # for loop through the event queue
        for event in pygame.event.get():
            # Check for KEYDOWN event; KEYDOWN is a constant defined in pygame.locals, which we imported earlier
            if event.type == KEYDOWN:
                # If the Esc key has been pressed set running to false to exit the main loop
                if event.key == K_ESCAPE:
                    running = False
                if event.key == K_DOWN:
                    if not pos > 0:
                        pos = pos+1
                if event.key == K_UP:
                    if not pos <= 0:
                        pos = pos-1
                if event.key == K_SPACE or event.key == K_RETURN:
                    if pos == 0:
                        caveSelectionScreen = True
                    if pos == 1:
                        viewHighScoreScreen = True
                    mainMenu = False

            elif event.type == QUIT:
                running = False
    # resets pos
    pos = 0
    cavenum = 0
    while running and caveSelectionScreen:
        gi.displaycaveselectionscreen(pos)
        # for loop through the event queue
        for event in pygame.event.get():
            # Check for KEYDOWN event; KEYDOWN is a constant defined in pygame.locals, which we imported earlier
            if event.type == KEYDOWN:
                # If the Esc key has been pressed set running to false to exit the main loop
                if event.key == K_ESCAPE:
                    running = False
                if event.key == K_DOWN:
                    if not pos > 3:
                        pos = pos + 1
                if event.key == K_UP:
                    if not pos <= 0:
                        pos = pos - 1
                if event.key == K_BACKSPACE:
                    caveSelectionScreen = False
                    mainMenu = True
                if event.key == K_SPACE or event.key == K_RETURN:
                    caveSelectionScreen = False
                    enterNameScreen = True
                    cavenum = pos
            # Check for QUIT event; if QUIT, set running to false
            elif event.type == QUIT:
                running = False

    while running and viewHighScoreScreen:
        gi.displayhighscores(hs.getScores())
        # for loop through the event queue
        for event in pygame.event.get():
            # Check for KEYDOWN event; KEYDOWN is a constant defined in pygame.locals, which we imported earlier
            if event.type == KEYDOWN:
                # If the Esc key has been pressed set running to false to exit the main loop
                if event.key == K_ESCAPE:
                    running = False
                if event.key == K_BACKSPACE:
                    viewHighScoreScreen = False
                    mainMenu = True

            # Check for QUIT event; if QUIT, set running to false
            elif event.type == QUIT:
                running = False

    name = ""
    iserror = False
    while running and enterNameScreen:
        gi.displayname(name, iserror)
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_RETURN and len(name) > 0:
                    currentPlayer = player.Player(name)
                    name = ''
                    enterNameScreen = False
                    gameScreen = True
                elif event.key == K_RETURN and len(name) == 0:
                    iserror = True
                elif event.key == K_ESCAPE:
                    running = False
                elif event.key == K_BACKSPACE:
                    name = name[:-1]
                else:
                    name += event.unicode

                    # Check for QUIT event; if QUIT, set running to false
            elif event.type == QUIT:
                running = False

    if gameScreen:
        currentcave = cave.Cave(cavenum)
        facing = 0
        hintclock = 0
        warningslist = []
        secret = currentgl.getSecret(currentcave.getCave())
        if random.randrange(10) == 2:
            secret = currenttrivia.getOldTrivia()

    while running and gameScreen:

        warningslist = currentgl.checkadjacents(currentcave.getCave())
        if len(warningslist)>0:
            gi.displayWarning(warningslist)


        gi.displayCave(currentgl.currentRoom, currentcave.getCave(), facing, currentPlayer.numCoins, currentPlayer.numTurns, currentPlayer.numArrows)
        if hintclock > 0:
            hintclock = hintclock-1
            gi.displayHint(hint)


        if secretclock > 0:
            secretclock = secretclock-1

            gi.displaySecret(secret)
            if secretclock ==0:
                secret = currentgl.getSecret(currentcave.getCave())
                if random.randrange(10) == 2:
                    secret = currenttrivia.getOldTrivia()

        if wumpusranawayclock>0:
            wumpusranawayclock = wumpusranawayclock-1
            gi.displaySecret("The Wumpus was scared of your trivia and ran away!")

        if nosecretclock>0:
            nosecretclock = nosecretclock-1
            gi.displaySecret("You didn't answer enough trivia questions correctly to earn a secret.")
        if noarrowclock>0:
            noarrowclock = noarrowclock-1
            gi.displaySecret("You didn't answer enough trivia questions correctly to earn more arrows")
        if wumpusmissclock>0:
            wumpusmissclock = wumpusmissclock-1
            gi.displaySecret("You missed, causing the Wumpus to run somewhere else in the cave.")





        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_u:
                    gameScreen = False
                    buyArrowScreen = True
                if event.key == K_k:
                    gameScreen = False
                    buySecretScreen = True
                #if event.key == K_n:
                #    secretclock = 240
                if event.key == K_x:
                    if currentPlayer.shootWumpus(currentcave.getCave(), currentgl.currentRoom, facing, currentgl.wumpusRoom) == "killed":
                        gameScreen = False
                        youWonScreen = True
                    elif currentPlayer.shootWumpus(currentcave.getCave(), currentgl.currentRoom, facing, currentgl.wumpusRoom) == "not":
                        currentPlayer.numArrows = currentPlayer.numArrows-1
                        wumpusmissclock = 120
                        currentgl.movewumpusarrow(currentcave.getCave())
                    if currentPlayer.numArrows<1:
                        lossReason = "The Wumpus knew you ran out of arrows and ate you."
                        gameScreen = False
                        youLostScreen = True


                # if event.key == K_t:
                #    currentgl.isWumpusInNext2(currentcave.getCave(), 2, currentgl.currentRoom)
                if event.key == K_ESCAPE:
                    running = False
                # arrow keys turn player
                if event.key == K_LEFT and facing > 0:
                    facing = facing-1
                elif event.key == K_LEFT and facing == 0:
                    facing = 5
                elif event.key == K_RIGHT and facing < 5:
                    facing = facing+1
                elif event.key == K_RIGHT and facing == 5:
                    facing = 0

                # 'a' and 'd' keys turn player

                elif event.key == K_a and facing > 0:
                    facing = facing-1
                elif event.key == K_a and facing == 0:
                    facing = 5
                elif event.key == K_d and facing < 5:
                    facing = facing+1
                elif event.key == K_d and facing == 5:
                    facing = 0

                elif event.key == K_SPACE:
                    print(currentgl.wumpusRoom)
                    warningslist = []
                    oldroom = currentgl.currentRoom
                    tempresult = currentgl.moveplayer(currentcave.getCave(), facing)
                    newroom = currentgl.currentRoom

                    #print(currentgl.wumpusRoom)
                    if tempresult == "wumpus":
                        gameScreen = False
                        wumpusTriviaScreen = True
                    if tempresult == "pit":
                        gameScreen = False
                        pitTriviaScreen = True

                    if oldroom != newroom:
                        hintclock = 60
                        hint = currenttrivia.gethint()
                        currentPlayer.incrementTurns()

                # if event.key == K_c:
                #    currentgl.movewumpus(currentcave.getCave())
            elif event.type == QUIT:
                running = False

    if wumpusTriviaScreen:
        questionNum = 0
        numCorrect = 0
        answers = currenttrivia.getshuffeledanswers()

    while running and wumpusTriviaScreen:

        gi.displaytrivia(currenttrivia.getquestion(), numCorrect, answers, questionNum, "wumpus")

        if numCorrect > 2:
            currentgl.movewumpus(currentcave.getCave())

            wumpusTriviaScreen = False
            gameScreen = True

            wumpusranawayclock = 180
        elif questionNum > 4:
            lossReason = "The Wumpus ate you!"
            wumpusTriviaScreen = False
            youLostScreen = True
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if currentPlayer.numCoins < 1:
                    lossReason = "You ran out of coins!"
                    buyArrowScreen = False
                    youLostScreen = True
                if event.key == K_ESCAPE:
                    running = False
                # 1-4 selects answer
                if event.key == K_1:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[0]):
                        numCorrect = numCorrect+1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                elif event.key == K_2:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[1]):
                        numCorrect = numCorrect+1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                elif event.key == K_3:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[2]):
                        numCorrect = numCorrect+1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                elif event.key == K_4:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[3]):
                        numCorrect = numCorrect+1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()

            elif event.type == QUIT:
                running = False




    if pitTriviaScreen:
        questionNum = 0
        numCorrect = 0
        answers = currenttrivia.getshuffeledanswers()

    while running and pitTriviaScreen:

        gi.displaytrivia(currenttrivia.getquestion(), numCorrect, answers, questionNum, "pit")

        if numCorrect > 1:
            currentgl.movewumpus(currentcave.getCave())
            currentgl.moveplayertospawn()
            pitTriviaScreen = False
            gameScreen = True
        if questionNum > 2 and numCorrect<2:
            lossReason = "You fell in a pit!"
            pitTriviaScreen = False
            youLostScreen = True
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if currentPlayer.numCoins < 1:
                    lossReason = "You ran out of coins!"
                    buyArrowScreen = False
                    youLostScreen = True
                if event.key == K_ESCAPE:
                    running = False
                # 1-4 selects answer
                if event.key == K_1:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[0]):
                        numCorrect = numCorrect+1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                elif event.key == K_2:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[1]):
                        numCorrect = numCorrect+1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                elif event.key == K_3:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[2]):
                        numCorrect = numCorrect+1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                elif event.key == K_4:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[3]):
                        numCorrect = numCorrect+1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()

            elif event.type == QUIT:
                running = False




    if buyArrowScreen:
        questionNum = 0
        numCorrect = 0
        answers = currenttrivia.getshuffeledanswers()
    while running and buyArrowScreen:
        gi.displaytrivia(currenttrivia.getquestion(), numCorrect, answers, questionNum, "pit")

        if numCorrect > 1:
            currentPlayer.buyArrow()
            buyArrowScreen = False
            gameScreen = True
        if questionNum > 2 and numCorrect<2:
            noarrowclock = 120
            buyArrowScreen = False
            gameScreen = True
        for event in pygame.event.get():

            if event.type == KEYDOWN:
                if currentPlayer.numCoins < 1:
                    lossReason = "You ran out of coins!"
                    buyArrowScreen = False
                    youLostScreen = True
                # 1-4 selects answer
                if event.key == K_1:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[0]):
                        numCorrect = numCorrect + 1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                elif event.key == K_2:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[1]):
                        numCorrect = numCorrect + 1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                elif event.key == K_3:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[2]):
                        numCorrect = numCorrect + 1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                elif event.key == K_4:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[3]):
                        numCorrect = numCorrect + 1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                if event.key == K_ESCAPE:
                    running = False

            elif event.type == QUIT:
                running = False





    if buySecretScreen:
        questionNum = 0
        numCorrect = 0
        answers = currenttrivia.getshuffeledanswers()
    while running and buySecretScreen:
        gi.displaytrivia(currenttrivia.getquestion(), numCorrect, answers, questionNum, "pit")

        if numCorrect > 1:
            secretclock = 240
            buySecretScreen = False
            gameScreen = True
        if questionNum > 2 and numCorrect < 2:
            nosecretclock = 120
            buySecretScreen = False
            gameScreen = True
        for event in pygame.event.get():

            if event.type == KEYDOWN:
                if currentPlayer.numCoins < 1:
                    lossReason = "You ran out of coins!"
                    buySecretScreen = False
                    youLostScreen = True
                # 1-4 selects answer
                if event.key == K_1:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[0]):
                        numCorrect = numCorrect + 1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                elif event.key == K_2:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[1]):
                        numCorrect = numCorrect + 1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                elif event.key == K_3:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[2]):
                        numCorrect = numCorrect + 1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                elif event.key == K_4:
                    questionNum = questionNum + 1
                    if currenttrivia.checkanswer(answers[3]):
                        numCorrect = numCorrect + 1
                    answers = currenttrivia.getshuffeledanswers()

                    currentPlayer.minusCoin()
                if event.key == K_ESCAPE:
                    running = False

            elif event.type == QUIT:
                running = False


    if youLostScreen:
        if currentPlayer.numCoins<0:
            currentPlayer.numCoins = 0
        score = currentPlayer.calculateScore(currentPlayer.name, cavenum, currentPlayer.numTurns, currentPlayer.numCoins, currentPlayer.numArrows, False)
        hs.setHighScore(score)
    while running and youLostScreen:
        gi.displayLossScreen(lossReason)

        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    running = False
                if event.key == K_SPACE or event.key == K_RETURN:
                    lossReason = ""
                    youLostScreen = False
                    viewHighScoreScreen = True

            elif event.type == QUIT:
                running = False


    if youWonScreen:
        if currentPlayer.numCoins < 0:
            currentPlayer.numCoins = 0
        score = currentPlayer.calculateScore(currentPlayer.name, cavenum, currentPlayer.numTurns, currentPlayer.numCoins, currentPlayer.numArrows, True)
        hs.setHighScore(score)
    while running and youWonScreen:
        gi.displayWinScreen()
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    running = False
                if event.key == K_SPACE or event.key == K_RETURN:
                    youWonScreen = False
                    viewHighScoreScreen = True

            elif event.type == QUIT:
                running = False