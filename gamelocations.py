import random
class GameLocations:
    def __init__(self):
        # check if all the numbers are different, then assign each of them to starting, pit, and bat
        self.batRooms = [-1,-1]
        self.pitRooms = [-1,-1]


        self.emptyRooms = []
        for i in range(30):
            self.emptyRooms.append(i+1)

        self.startingRoom = self.emptyRooms.pop(random.randrange(len(self.emptyRooms)))

        for i in range(2):
            self.batRooms[i] = self.emptyRooms.pop(random.randrange(len(self.emptyRooms)))
        for i in range(2):
            self.pitRooms[i] = self.emptyRooms.pop(random.randrange(len(self.emptyRooms)))

        self.currentRoom = self.startingRoom
        self.visitedRooms = []
        self.addvisited(self.currentRoom)
        self.wumpusRoom = self.emptyRooms.pop(random.randrange(len(self.emptyRooms)))

    def moveplayer(self, connections, facing):
        if connections[self.currentRoom-1][facing] > 0:
            self.currentRoom = connections[self.currentRoom-1][facing]
            self.addvisited(self.currentRoom)
        for i in range(2):
            if self.currentRoom == self.batRooms[i]:
                self.movebat(i)
                #print("bat moved")
                return "bat"
        for i in range(2):
            if self.currentRoom == self.pitRooms[i]:
                #print("pit")
                return "pit"
        if self.currentRoom == self.wumpusRoom:
            #print("wumpus")
            return "wumpus"
        return ""


    def movebat(self, batNum):
        tempnums = [self.currentRoom, self.batRooms[0], self.batRooms[1], self.pitRooms[0], self.pitRooms[1]]
        # moves bat
        tempnums[1+batNum] = random.randrange(30)
        while len(set(tempnums)) !=len(tempnums):
            tempnums[1 + batNum] = random.randrange(30)
        # moves player to new room
        self.currentRoom = tempnums[1+batNum]

        # moves bat again
        tempnums[1 + batNum] = random.randrange(30)
        while len(set(tempnums)) != len(tempnums):
            tempnums[1 + batNum] = random.randrange(30)

        self.batRooms[0] = tempnums[1]
        self.batRooms[1] = tempnums[2]
        # adds new room to visitedRooms list
        self.addvisited(self.currentRoom)

    def movewumpus(self, connections):
        num = random.randrange(2, 5)
        movedlist = []
        for i in range(num):
            # adds the current wumpus room to a temp array
            movedlist.append(self.wumpusRoom)
            for x in range(6):
                # checks to see if the wumpus has already moved there
                if connections[self.wumpusRoom-1][x] not in movedlist:
                    # moves wumpus to new room
                    self.wumpusRoom = connections[self.wumpusRoom-1][x]
                    #print(self.wumpusRoom)
                    # stops cycling through the current rooms connections as the wumpus has moved
                    break

    def movewumpusarrow(self, connections):
        movedlist = []
        for i in range(2):
            # adds the current wumpus room to a temp array
            movedlist.append(self.wumpusRoom)
            for x in range(6):
                # checks to see if the wumpus has already moved there
                if connections[self.wumpusRoom-1][x] not in movedlist:
                    # moves wumpus to new room
                    self.wumpusRoom = connections[self.wumpusRoom-1][x]
                    #print(self.wumpusRoom)
                    # stops cycling through the current rooms connections as the wumpus has moved
                    break

    def addvisited(self, visitedNum):
        self.visitedRooms.append(visitedNum)

    def checkadjacents(self, connections):
        warnings = []
        adjacents = connections[self.currentRoom-1]
        for i in range(6):
            for x in range(2):
                if adjacents[i] == self.batRooms[x]:
                    if self.batRooms[x] !=0:
                        warnings.append("Bats Nearby")
                        break
            for x in range(2):
                if adjacents[i] == self.pitRooms[x]:
                    if self.pitRooms[x] != 0:
                        warnings.append("I feel a draft")
                        break
            if adjacents[i] == self.wumpusRoom:
                if adjacents[i] != 0:
                    warnings.append("I smell a Wumpus!")
        return warnings

    def moveplayertospawn(self):
        self.currentRoom = self.startingRoom

    def getSecret(self, connections):
        num = random.randrange(5)
        if num ==0:
            return "A bat is in room " + str(self.batRooms[0])

        elif num == 1:
            return "A pit is in room " + str(self.pitRooms[0])
        elif num==2:
            if self.isWumpusInNext2(connections, 2, self.currentRoom):
                return "The Wumpus is within two rooms."
            else:
                return "The Wumpus is further than two rooms."
        elif num == 3:
            return "The Wumpus is in room " + str(self.wumpusRoom)
        elif num == 4:
            return "Your current room is " + str(self.currentRoom)

    def isWumpusInNext2(self, connections, step, currentRoom):
        if self.wumpusRoom == currentRoom:
            return True
        if step == 0:
            return False
        nextRooms = []
        for i in range(6):
            if not connections[currentRoom-1][i] == 0:
                if connections[currentRoom-1][i] != currentRoom:
                    nextRooms.append(connections[currentRoom-1][i])
        for i in range(len(nextRooms)):
            if self.isWumpusInNext2(connections, step - 1, connections[currentRoom-1][i]):
                return True
        return False




