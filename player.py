import pygame


class Player(pygame.sprite.Sprite):
    def __init__(self, name):
        pygame.sprite.Sprite.__init__(self)
        self.numCoins = 0
        self.coinsCollected = 0
        self.numArrows = 3
        self.numTurns = 0
        self.name = name

    def incrementTurns(self):
        if self.coinsCollected<100:
            self.numCoins = self.numCoins + 1
            self.coinsCollected = self.coinsCollected+1
        self.numTurns = self.numTurns+1

    def minusCoin(self):
        self.numCoins = self.numCoins - 1

    def buyArrow(self):
        self.numArrows = self.numArrows+2

    def shootWumpus(self, connections, currentroom, facing, wumpusroom):
        if connections[currentroom - 1][facing] > 0:
            if connections[currentroom-1][facing] == wumpusroom:
                return "killed"
            if connections[currentroom-1][facing] != wumpusroom:
                return "not"
        else:
            return "cant"

    def calculateScore(self, name, cavename, turns, coins, arrows, killed):
        killedbonus = 0
        if killed:
            killedbonus = 50
        string = "NOTKILLED"
        if killed:
            string = "KILLED"
        scorestr = str(100-turns+coins-(5*arrows) + killedbonus) + " " + name + " " + str(cavename) + " " + str(turns) + " " + str(coins) + " " + str(arrows) + " " + string + "\n"
        return scorestr

